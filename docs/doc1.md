---
id: doc1
title: DComp Testbed Design
sidebar_label: Hardware Design
---

The DComp testbed is best understood by the networks it's composed of. The testbed has two primary networks, the **_control network_** and the **_experiment network_**. The control network is for experiment synthesis and setup, things like imaging nodes with operating systems and getting data to the right places before experimentation begins. The control network is also used for instrumentation within experiments so that measurement traffic does not interfere with experimental results.

## Control Network

The control network is depicted in the diagram below.

![img](/img/cnet.png)


## Experiment Network

The experiment network is depicted in the diagram below.

![img](/img/xnet.png)
