---
id: tour
title: DCompTB Tour
sidebar_label: DCompTB Tour
---

## Hardware Specs

The DComp testbed is comprised of the following.

- 1200 quad-core [Minnowboards](https://minnowboard.org/minnowboard-turbot-dual-e/technical-specs).
- 240 [RCC-VE](http://www.adiengineering.com/products/rcc-ve-board/) embedded network processing modules.
- A [control network](#control-networks) plane for experiment infrastructure and orchestration.
- An [experiment network](#experiment-networks) plane for experiment traffic.
- 320 terabytes of general [mass storage](#mass-storage).
- 8 terabytes of fast [image](#os-images) storage.
- [Console server](#console-access) coverage for all nodes in the testbed.
- A 1 tbps [network emulation](#network-emulation) plane.

The testbed brings these components together to form a cohesive experimentation tool that allows resarchers to rapidly design, deploy and analyze complex network experiments.

## Using the Testbed

The anatomy of a DCompTB experiment is depicted below.

![img](/img/anatomy.png)

The first thing to note is that DCompTB is a part of a distributed testbed ecosystem. The DComp testbed provides hardware resources such as nodes, mass storage and specialized emulation machines. However, it is a part of a broader ecosystem that performs key functions such **realization**: mapping and allocating resources to experiments, **materialization**: provisioning orchestration for a distributed set of resources, and an **experiment management platform**: containerized access to experiment control networks.

Using the testbed boils down to a few key activities

- [Experiment Definition](#experiment-definition) involves expressing a topology, provisioning the resources necessary to run the experiment and, defining an entry point to run the experiment. 

- [Realization](#realization) is the process of mapping an experiment definition to a set of resources. The mechanical act of mapping is done algorithmically by the [Merge](https://www.mergetb.net) portal. However, realization is typically an iterative process. In each iteration the researcher specifies a topology as a set of constraints and the realization engine calculates a mapping that satisfies those constraints. If the calculated result is not what the researcher wanted, or if a mapping was not possible given available resources - the researcher can adjust the constraints and resubmit. Realization is a fast and lightweight process designed for rapid iteration.

- [Materialization](#materialization) is the process of provisioning the resources allocated in a realization according to the specification laid out in a topology definition. This ranges from imaging nodes with a particular OS to setting up link parameters on an emulation server and many things in between. The set of available materialization parameters are device dependent. The set of supported device parameters may be found [here](/docs/mzparam.html).

- [Execution](#execution) is the process of running an experiment. During experiment development execution is typically done by a researcher interacting with experiment resources through command line, web and scripting interfaces. As experiments develop, these ad-hoc techniques give way to orchestration. Each experiment has an entry point hook that is executed once the experiment has been fully materialized. This can be a call-out to a user defined orchestration that uses popular tools such as Ansible or SaltStack, or it can simply be a shell script or user provided executable.

All of the above activities are supported by [experiment development containers](#experiment-development-containers) (**XDC**). These are containers that are associated with a particular target experiment that can be spun up on demand. XDCs are placed on the target experiments control network. They are accessible via publicly routable addresses that are assigned DNS names as follows `<container-id>.<experiment-id>.<project-id>.mergetb.io`. XDCs are the gateways to experiments. They can also be used to provision mass storage volumes with the resources necessary to run an experiments independent of experiment lifetime.


### Experiment Definition

Topology expression is typically done programmatically using high-level languages such as Python. There is a core underlying representation called [xir](https://gitlab.com/mergetb/xir) that high-level language bindings generate - so custom interfaces such as GUIs based on this representation are possible.

#### Topology Expression

Here we show a simple two node topology using the Python bindings by example. In this topology we want a node tht runs fedora with some specific hardware features and file system mounts connected to a node running Ubuntu.

![](/img/props.png)

```python
from xir import Network, mbps, ge, gB, lt, eq

topo = Network({'name': '2-node'})

a = topo.node({
    'cpu': { 'cores': ge(4) },
    'memory': { 'capacity': ge(gB(4)) }
}).configure({
    'name': 'a',
    'image': 'fedora28',
    'volume': [
        { 'capacity': eq(gB(500)), 'mount': '/space' },
        { 'name': 'project47', 'mount': '/p47' }
    ]
})

b = topo.node().configure({
    'name': 'b',
    'image': 'ubuntu'
})

topo.connect([a, b], { 'bandwidth': lt(mbps(10)) })
```

We'll pick this apart piece by piece. The import is from the `xir` library. This library is the _experiment intermediate representation_ library. It's available [here](https://github.com/ceftb/xir).

We begin by defining a network with the name `2-node`.

```python
topo = Network({'name': '2-node'})
```

The network has a number of methods associated with it for adding nodes, links and even recursive subnetworks. We use the `node` method to add our first node.

```python
a = topo.node({
    'cpu': { 'cores': ge(4) },
    'memory': { 'capacity': gB(4) }
}).configure({
    'name': 'a',
    'image': 'fedora28',
    'volume': [
        { 'capacity': eq(gB(500)), 'mount': '/space' },
        { 'name': 'project47', 'mount': '/p47' }
    ]
})
```

The expression of a node is divided into two parts its **_elements_** and its **_configuration_**. The elements of a node are a set of constraints that describe the valid range of resources that can be used to underpin the node for experimentation purposes. In this example we see that the experiment requires that node `a` must have greater than or equal to `4` processor cores and `8` gigabytes of memory. Any resource node that satisfies these constraints is considered a candidate for realizing node `a`. Based on the DComp resources, this limits potential candidates to RCC-VE nodes.

Next we see the configuration of node `a`. Here we specify that the node should be imaged with an OS image called 'fedora28' and that two volumes should be present. The first volume is required to be 500 gigabytes and the second one must have the name 'project47'. Both volume specifications specify where the volumes should be mounted on node `a`'s base filesystem.

The configuration specification also has the potential to reduce the realization space of a node. For example, the 'fedora28' image may not be valid for all resources that satisfy the elemental constraints. Likewise volume mounting is likely not supported on many node types.

The next node has no elemental constraints, we will settle for anything that can support an image called 'ubuntu'. All resources are required to support the 'name' configuration.

```python
b = topo.node().configure({
    'name': 'b',
    'image': 'ubuntu'
})
```

Finally  we connect nodes `a` and `b` via the `connect` method of the `Network` class.

```python
topo.connect([a, b], { 'bandwidth': lt(mbps(10)) })
```

This creates a `link` object and adds it to the topology. We introduce a single elemental constraint here that requires the bandwidth be less than `10` megabits per second. For DCompTB this constraint means that a network emulator will be placed transparently between nodes `a` and `b` that ensures this property.

#### Resource Provisioning

Resources can include many things. Including but not limited to

<table style="border: none">
<tr style="border: none">
<td style="border: none; vertical-align: middle">
<ul>
<li>Configuration files</li>
<li>Keys</li>
<li>Databases</li>
<li>Software</li>
<li>File systems</li>
</ul>
</td>
<td style="border: none; vertical-align: middle">
<img src='/img/provision.png' height='150px' />
</td>
</tr>
</table>

In order to ensure resources are accessible to experiments at materialization and execution time, DCompTB provides the ability for [static storage volumes](#static) to be provisioned independent of experiment lifetime and populated with the necessary resources via an XDC.

#### Entry Point

Each XDC has an experiment entry point. This entry point is a script located at the well known path `/run.sh`. An experiment can have zero or many XDCs. If an experiment has no XDC then no entry point is called. If there is one XDC, the entry point from that container is called. If there are many XDCs the default behavior is for the most recent XDCs entry point to be used. However, the user may override this behavior by explicitly specifying the id of the desired XDC to be used.

Users may explicitly specify the XDC to use via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)

### Realization

Realization is the process in which researchers express a topology as a set of requirements and the testbed attempts to realize those requirements as a set of satisfying resources. The realization is a bijective mapping (sometimes referred to as a virtual network embedding) between experiment nodes and links and testbed nodes and paths `(experiment-node, experiment-link) -> (testbed-node, testbed-network-path)`.  Realization is designed to be a back and forth process where the researcher refines the constraints over a set of iterations until the desired set of resources are selected. The general concept is depicted in the diagram below.

![](/img/realize.png)

When a realization is calculated, the selected resources are reserved in the pending state. The researcher may then accept or reject the realization. If the realization is accepted, the resources transition from the pending to the reserved state and belong to the experiment until explicitly freed or the lease on the resources runs out. If the realization is rejected, all resources are released. If a realization is not accepted within 1 minute, the realization will be automatically rejected.

Users may perform a realization via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)

### Materialization

Materialization is the process by which a researcher transforms an experiment from a set of allocations to a ticking, breathing, experiment. Materialization is a much heavier weight process than realization as nodes must be imaged, booted, and configured. However, because DCompTB is made up of embedded components and has a high performance imaging system - materializations that involve only DCompTB resources are quite fast, typically well under 1 minute.

![](/img/materialize.png)

Materializations are always based on a particular realization. Thus a realization id must be provided when performing a materialization. An experiment may have multiple realizations and a realization may have multiple materializations.

Users may perform a materialization via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)

### Execution

Execution at face value is a simple task: tell the testbed to execute the `/run.sh` script in a particular XDC. In practice developing the systems behind the `/run.sh` script is one of the most challenging aspects of experiment design. Here are a few best practices and tips for orchestrating complex experiments.

- Use an established orchestration tool (Ansible, Chef, Puppet)
- Use an established monitoring tool (Fluentd, Prometheus, Telegraf, Graphana)
- Ensure that experiment orchestration traffic runs over the control network to avoid contaminating experimental results
- Ensure that monitoring and instrumentation traffic runs over the control network
- Tunnel any ports desired into an XDC to provide tools access to the experiment and control network

Users may trigger an execution via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)

### Experiment Development Containers

Experiment development containers (XDC) create a focal point for experiment development. They may be created on demand and are mostly independent of experiment lifetime. We say "mostly" because if there is a designated entry point for the XDC, the materialization system will launch it once a materialization has completed. If an experiment has been materialized, any active XDCs have access to experiment's control network. If there are multiple materializations of an experiment, a particular materialization may be specified for the XDC to be attached to. An XDC can only be attached to one materialization at a time. However, the attachment association may be changed at any time.

Each XDC is reachable over the internet at the DNS name

```
<container-name>.<experiment>.<project>.mergetb.io
```

Users may create, launch and (re)attach XDCs via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)

## Specific Capabilities

### Control Networks

Each materialization gets a dedicated, isolated and flat control network.

<img src="/img/mzctl.png" style="max-width: 85%"></img>

The control network provides _XDC_ access to the following:

- A dedicated physical network port to experiment nodes
- Expirement node console ports that are completely independent of the OS and network subsystems on that node - a true low level UART console port

The control network provides _node_ access to the following:

- Other nodes on the same control network
- Mass storage resources
- Image servers
- DHCP servers
- DNS servers

Each experiment materialization gets an isolated control network. The network is synthesized as a part of the materialization process. The control network isolation is based on VLANs and is thus not a fully encapsulated network (e.g. user defined VLANs will not work on the control network unless Q-in-Q is used explicitly).

Network addresses are automatically assigned on the control network through DHCP. The DHCP subnet that is used is configurable as a topology level materialization parameter. The default subnet is `172.16.2.0/16`. Addresses below `172.16.2.0/16` (`172.16.0.1 - 172.16.2.255`) are reserved - independent of subnet specification. This address block is used for mass storage devices and console servers. Manual assignment of these addresses on a materialization's control network may result in loss of these services or undefined experiment behavior.

DNS is provided on the control network. Node addresses may be resolved using `node-name` or `node-name.experiment-name`.  The DNS and DHCP servers on each control network are located at the following well known address **172.16.0.1**.

### Experiment Networks

The way an experiment network is implemented for a materialization is shown in the diagram below.

<img src="/img/mzxp.png" style="max-width: 85%"></img>

Each experiment materialization gets a set of isolated experiment networks, one for each link. The word link is used in the generalized sense that links may be point to point or a general substrate with many endpoints.

When link properties are defined that require emulation, network emulation resources are placed transparently between all endpoints on a link that ensure the properties are satisfied.

No addressing or name resolution of any kind is provided by default on experiment networks. Only layer 2 connectivity is provided. However, canned solutions for [experiment DHCP](#todo) and [experiment DNS](#todo) are available to experimenters.

Experiment networks are implemented using full encapsulation virtualization (VXLAN) meaning that the isolation should be totally transparent to experiment nodes. VLANs and nested VXLANs will work as expected on experiment networks.

### Console Access

Console access is available from XDCs. The console for any node is available through SSH at the address `<node-name>.console`. Even though the consoles are accessed through SSH, these are indeed real UART level consoles - so you can do things like debug early-boot kernel crashes or have network independent access to experiment nodes. Firmware access is restricted at this time, but may become available at a later date or on specific request.

![](/img/console.png)

Console access is based on SSH keys that are automatically placed in an XDC at the time it is bound to a materialization.

### Mass Storage

DCompTB mass storage volumes come in two modes, [static](#static) and [ephemeral](#ephemeral). Each of these were shown by example in the [experiment definition](#experiment-definition) section.  Static volumes exist independent of experiment lifetime, and ephemeral models live and die with experiment materialization and dematerialization. Access modes are supported on both static and ephemeral volumes. They include `read-only`, `write-only` and `read-write`. 

#### Ephemeral
For ephemeral volumes, each materialization gets a new and unique volume. The volume is initialized to an empty state. Users may specify a target file system for the volume, and the volume will be formatted with that file system at materialization time.

#### Static
For static volumes, multiple materialization behavior is configurable. The default mode is `instanced` which behaves in the same was as ephemeral volumes, one unique instance of the volume per materialization. A `singleton` mode is also available which means all materializations share a single volume. It's suggested to use `singleton` containers in `read-only` mode to avoid non-deterministic concurrent access issues.

Mass storage is provided in two different ways: volumes or block devices.

![](/img/stor.png)

#### Network File Systems
The storage exists as a file system that is hosted on the DCompTB Ceph servers. If it is a static volume, it can be accessed at any time from an XDC. This is useful if you need to pre-provision the volume with resources before materialization.

Users may create static volumes via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)


### OS Images

DCompTB has a fast OS imaging system called [sled](https://gitlab.com/mergetb/sled). We provide a set of standard base images, or users can provide their own.

#### Standard
We provide a standard set of base images:

- Ubuntu
- Fedora
- Debian
- CentOS
- FreeBSD

Supported versions of these images are exactly what is supported upstream. EOL images are not supported.

#### Custom

DCompTB uses the QEMU RAW image format. Users are free to make their own images using tools such as Packer. The only requirement is **images must have [cloud-init](https://cloud-init.io) installed**. See the [cloud-init docs](https://cloudinit.readthedocs.io/en/latest/) for more details.

Users may upload custom images via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)

#### Imaging API

DCompTB exposes an API for managing node images dynamically, this includes wiping and reimaging a node without having to completely rematerialize the experiment.

Users can interact the sled imaging system via

- [The Merge Web Interface](#todo)
- [The Merge API](#todo)
- [The Merge CLI](#todo)


### Network Emulation




