/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + 'docs/' + (language ? language + '/' : '') + doc;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? language + '/' : '') + doc;
  }

  render() {
    const currentYear = new Date().getFullYear();
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <div>
            <center> 
            <h5>Community</h5>
            <a href="https://dcomptb.slack.com/">Project Chat</a>
            </center>
          </div>
          <div>
            <center> 
            <h5>Built on Merge</h5>
            <a href="https://www.mergetb.org">
              <img className="footerlogo" src="/img/merge-light.svg" className="mergeLogo" />
            </a>
            </center>
          </div>
          <div>
            <center> 
            <h5>More</h5>
            <a href={this.props.config.baseUrl + 'blog'}>Blog</a>
            </center>
          </div>
        </section>

        <section className="copyright">{this.props.config.copyright}</section>
      </footer>
    );
  }
}

module.exports = Footer;
