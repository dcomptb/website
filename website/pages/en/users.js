/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');
const Container = CompLibrary.Container;

const siteConfig = require(process.cwd() + '/siteConfig.js');

class Users extends React.Component {
  render() {
    if ((siteConfig.users || []).length === 0) {
      return null;
    }
    const editUrl = siteConfig.repoUrl + '/edit/master/website/siteConfig.js';
    const showcase = siteConfig.users.map((user, i) => {
      return (
        <div className="performer">
            <table className="perftable">
            <tr className="perftable">
            <td className="perftable">
            <a href={user.infoLink} key={i}>
            <img src={user.image} alt={user.caption} title={user.caption} />
            </a>
            </td>
            <td className="perftable">
            <strong>{user.title}</strong> 
            <br />
            <div dangerouslySetInnerHTML={{__html: user.blurb}} />
            </td>
            </tr>
            </table>
        </div>
      );
    });

    return (
      <div className="mainContainer soloContainer">
        <Container padding={['bottom', 'top']}>
          <div className="projectDetail">
            <div className="prose">
              <h1>DComp Testbed Users</h1>
            </div>
              <div>{showcase}</div>
            </div>
        </Container>
      </div>
    );
  }
}

module.exports = Users;
