/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// See https://docusaurus.io/docs/site-config.html for all the possible
// site configuration options.

/* List of projects/orgs using your project for the users page */
const users = [];
const _users = [
  {
    caption: 'DARPA',
    image: '/img/darpa.jpg',
    infoLink: 'https://www.darpa.mil/program/dispersed-computing',
    pinned: true,
    title: "DComp: Dispersed Computing Program",
    blurb: 'In the current art, users with significant computing requirements have typically depended on access to large, highly shared data centers to which they backhaul their data (e.g., images, video, or network log files) for processing. However, in many operational scenarios, the cost and latency of this backhaul can be problematic, especially when network throughput is severely limited or when the user application requires a near real-time response. In such cases, users’ ability to leverage computing power that is available “locally” (in the sense of latency, available throughput, or similar measures that are relevant to the user or mission) could substantially improve application performance while reducing mission risk. <br /><br /> The Dispersed Computing program seeks scalable, robust decision systems that enable secure, collective tasking of computing assets in a mission-aware fashion by users with competing demands, and across large numbers of heterogeneous computing platforms. Dispersed Computing seeks to design systems able to operate in environments where network connectivity is highly variable and degraded. The envisioned computing paradigm aims to enable the strategic, opportunistic movement of code to data, and data to code, in a fashion that best suits user, application, and mission needs.'
  },
  {
    caption: 'BAE Systems',
    image: '/img/bae.jpg',
    infoLink: 'https://baesystems.com',
    pinned: true,
    title: "NEBULA: Network Back Haul Layered Architecture",
    blurb: "The NEBULA project aims to ..."
  },
  {
    caption: 'LGS Innovations',
    image: '/img/lgs.jpg',
    infoLink: 'https://www.lgsinnovations.com/',
    pinned: true,
    title: "DSPRS: Dispersed Computing via Successive Refinement and Pricing Resilience and Scale",
    blurb: "The DSPRS project aims to ..."
  },
  {
    caption: 'Carnegie Mellon University',
    image: '/img/cmu.jpg',
    infoLink: 'https://www.cs.cmu.edu/',
    pinned: true,
    title: "Dispersed Computing via Agile Dimorphic Execution for Dispersed Computing",
    blurb: "Agile dimorphic execution ..."
  },
  {
    caption: 'Perspecta Labs',
    image: '/img/perspecta.png',
    infoLink: 'https://www.perspectalabs.com/',
    pinned: true,
    title: "Dispersed Computing via Programmable Networks Enabled by Fast In-Path Analytics",
    blurb: "Programmable networks ..."
  },
  {
    caption: 'Raytheon',
    image: '/img/raytheon.png',
    infoLink: 'https://www.raytheon.com/',
    pinned: true,
    title: "GNAT: Generalized Network Assisted Transport",
    blurb: "GNAT aims to ..."
  },
  {
    caption: 'Raytheon',
    image: '/img/raytheon.png',
    infoLink: 'https://www.raytheon.com/',
    pinned: false,
    title: "MAP: Mission-Oriented Adaptive Placement of Task and Data",
    blurb: "MAP aims to ..."
  },
  {
    caption: 'University of Southern California',
    image: '/img/usc.png',
    infoLink: 'https://www.cs.usc.edu/',
    pinned: true,
    title: "Adaptive Pricing and Coding for Dispersed Computing (APaC)",
    blurb: "APaC aims to ..."
  },
];

const siteConfig = {
  title: 'DCompTB' /* title for your website */,
  tagline: 'The Dispersed Computing Program Testbed',
  url: 'https://dcomptb.net' /* your website url */,
  baseUrl: '/' /* base url for your project */,
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  // Used for publishing and more
  projectName: 'DCompTB',
  organizationName: 'ISI Testbed Group',
  // For top-level user or org sites, the organization is still the same.
  // e.g., for the https://JoelMarcey.github.io site, it would be set like...
  //   organizationName: 'JoelMarcey'

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    //{doc: 'tour', label: 'Docs'},
    {blog: true, label: 'News'},
  ],

  // If you have users set above, you add it here:
  users,

  /* path to images for header/footer */
  headerIcon: 'img/logo-light.svg',
  footerIcon: 'img/logo.svg',
  favicon: 'img/logo.png',

  /* colors for website */
  colors: {
    primaryColor: '#164D82',
    secondaryColor: '#205C3B',
  },

  /* custom fonts for website */
  /*fonts: {
    myFont: [
      "Times New Roman",
      "Serif"
    ],
    myOtherFont: [
      "-apple-system",
      "system-ui"
    ]
  },*/

  // This copyright info is used in /core/Footer.js and blog rss/atom feeds.
  copyright:
    'Copyright © ' +
    new Date().getFullYear() +
    ' Information Sciences Institute',

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags
  scripts: ['https://buttons.github.io/buttons.js'],

  /* On page navigation for the current documentation page */
  onPageNav: 'separate',

  /* Open Graph and Twitter card images */
  ogImage: 'img/logo.png',
  twitterImage: 'img/logo.png',

  // You may provide arbitrary config keys to be used as needed by your
  // template. For example, if you need your repo's URL...
  //   repoUrl: 'https://github.com/facebook/test-site',
};

module.exports = siteConfig;
